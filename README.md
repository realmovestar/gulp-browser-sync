브라우저 싱크
--------------------------
특정 디렉토리를 모니터링해서 변경이 발생하면 브라우저를 새로고침 해줌

## 설정
gulpfile.js에 watchTarget에 모니터링할 디렉토리 설정

## 사용방법
- nodejs 설치

- glup 설치
npm install -g gulp

- 모듈 설치
npm install

- 실행
gulp
