var gulp = require('gulp');
var bs = require('browser-sync').create();

var watchTarget = "f:/workspace/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/cafe-webapp/**/*";

gulp.task('browser-sync', function() {
    bs.init({
        proxy: "local.gamcle.com"
    });

    gulp.watch(watchTarget, bs.reload);
});

gulp.task('default', ['browser-sync']);
